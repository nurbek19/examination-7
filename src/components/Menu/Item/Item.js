import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div className="item" onClick={props.addItem}>
            <h4>{props.name}</h4>
            <p>Price: {props.price} KGS</p>
        </div>
    )
};

export default Item;