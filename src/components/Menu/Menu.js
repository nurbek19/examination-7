import React from 'react';
import Item from './Item/Item';
import './Menu.css';

const Menu = props => {
    const itemsArray = [];
    const itemsNames = Object.keys(props.itemsNames);
    const itemsObjects = Object.values(props.itemsAmount);
    let index = 0;

    for (let i = 0; i < itemsNames.length; i++) {
        let itemName = itemsNames[i];
        let itemPrice = itemsObjects[i].price;
        index++;

        itemsArray.push(
            <Item
                key={index}
                name={itemName}
                price={itemPrice}
                addItem={() => props.addItem(itemName)}
            />
        );
    }

    return (
        <div className="menu">
            {itemsArray}
        </div>
    )
};

export default Menu;