import React from 'react';
import OrderItem from './OrderItem/OrderItem';
import './OrderList.css';

const OrderList = props => {
    const orderItemsArray = [];
    const orderItemsNames = Object.keys(props.itemsNames);
    const orderItemsObjects = Object.values(props.itemsAmount);
    const orderClass = ['orders'];
    const totalStyle = {visibility: 'hidden'};
    let index = 0;

    for (let i = 0; i < orderItemsNames.length; i++) {
        let orderItemName = orderItemsNames[i];
        let orderItemAmount = orderItemsObjects[i].amount;
        let orderItemPrice = orderItemsObjects[i].price;

        if (orderItemAmount > 0) {
            orderClass.push('hide');
            totalStyle.visibility = 'visible';
            index++;

            orderItemsArray.push(
                <OrderItem
                    key={index}
                    name={orderItemName}
                    amount={orderItemAmount}
                    price={orderItemPrice}
                    remove={() => props.remove(orderItemName)}
                />
            );
        }
    }

    return (
        <div className={orderClass.join(' ')}>
            <div className="empty">
                <p>Order is empty!</p>
                <p>Please add some items!</p>
            </div>
            {orderItemsArray}
            <p style={totalStyle}>Total price: {props.totalPrice}</p>
        </div>
    )
};

export default OrderList;