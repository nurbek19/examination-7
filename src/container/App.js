import React, {Component} from 'react';
import Menu from '../components/Menu/Menu';
import OrderList from '../components/OrderList/OrderList'
import './App.css';

class App extends Component {
    state = {
        menuItems: {
            hamburger: {amount: 0, price: 80},
            cheeseburger: {amount: 0, price: 90},
            fries: {amount: 0, price: 45},
            coffee: {amount: 0, price: 70},
            tea: {amount: 0, price: 50},
            cola: {amount: 0, price: 40}
        },
        totalPrice: 0
    };

    addItem = (name) => {
        const menuItems = {...this.state.menuItems};
        let currentItem = {...menuItems[name]};

        currentItem.amount++;

        const totalPrice = this.state.totalPrice + currentItem.price;

        menuItems[name] = currentItem;

        this.setState({menuItems, totalPrice})
    };

    remove = (name) => {
        const menuItems = {...this.state.menuItems};
        let totalPrice = this.state.totalPrice;
        let currentItem = {...menuItems[name]};

        totalPrice = totalPrice - currentItem.price * currentItem.amount;
        currentItem.amount = 0;
        menuItems[name] = currentItem;

        this.setState({menuItems, totalPrice});
    };



    render() {
        return (
            <div className="App">
                <Menu
                    itemsNames={this.state.menuItems}
                    itemsAmount={this.state.menuItems}
                    addItem={this.addItem}
                />

                <OrderList
                    itemsNames={this.state.menuItems}
                    itemsAmount={this.state.menuItems}
                    totalPrice={this.state.totalPrice}
                    remove={this.remove}
                />
            </div>
        );
    }
}

export default App;
