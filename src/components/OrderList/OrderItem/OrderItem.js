import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
    return (
        <div className="order">
            <div className="order-item">
                <p>{props.name}</p>

                <div>
                    <span>x {props.amount}</span>
                    <span>{props.price} KGS</span>
                    <span className="delete-item" onClick={props.remove}>remove</span>
                </div>
            </div>
        </div>
    )
};

export default OrderItem;